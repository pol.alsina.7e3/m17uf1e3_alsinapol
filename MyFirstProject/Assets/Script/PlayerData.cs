using UnityEngine;
using UnityEngine.UI;

public enum HabilityGiant
{
    Augment,
    Decrease,
    GiantTime,
    Cooldown,
    Nothing

}
public enum TypePlayer
{
    Sorcerer,
    Samurai,
    Citizen,
    Warrior
}
public class PlayerData : MonoBehaviour
{
    

    public TypePlayer type = TypePlayer.Citizen;
    public string PlayerName;
    public float Height=1f;
    public float Weight = 50f;
    [SerializeField]
    private int _yearsOld = 20;
    public int[] Stats;
    [SerializeField]
    //private Sprite _sprite;   
    public Transform MyTransform;
    public Sprite[] character;
    private int frameSprite;
    private int _counter;
    private int _frameRate;
    public float distance;
    private float _speedAtenuador = 6f;
    private float _speed = 0.1f;
    private GameObject putName;
    private Text stringName;
    [SerializeField] private float _rightBoundary = 0f;
    [SerializeField] private float _leftBoundary = 0f;
    private float dirX = -1f;
    private float _bigAtenuator = 2f;
    private float _velocityBig;
    private float _originalSize;
    public HabilityGiant hG;
    private bool _sacleSize = true;
    private float _timerBig;
    public float BigDuration = 5f;
    private bool _animate = true;
    private float _timer = 0f;

    private void Awake()
    {
        putName = GameObject.Find("name");
        stringName = putName.GetComponent<Text>();
        PlayerName = stringName.text;
        Debug.Log(PlayerName);
    }

    void Start()
    {
        
        // sPn.PlayerNameShowOnText();
        _counter = 0;
        _frameRate = 350;
        //Debug.Log(this.gameObject.GetComponent<SpriteRenderer>().sprite.name);
        transform.localScale = new Vector3(Height, Height, 0);
        _originalSize = transform.localScale.x;
        hG = HabilityGiant.Nothing;                
    }

    void Update()
    {
        
        PlayerBoundaries();
        CalculateSpeed();
        SpriteFlip();
        PlayerMove();
        SpriteAnimate();
        if (_sacleSize)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                hG = HabilityGiant.Augment;
            }
        }
        switch (hG)
        {
            case HabilityGiant.Augment:
                BigHabilty();
                break;
            case HabilityGiant.Decrease:
                DecHab();
                break;
            case HabilityGiant.GiantTime:
                TimerBig();
                break;
            case HabilityGiant.Cooldown:
                Cooldown();
                break;
        }

    }
    private void PlayerBoundaries()
    {
        if (transform.position.x >= _rightBoundary)
        {
            transform.position = new Vector3(_rightBoundary, transform.position.y, 0);
        }
        else if (transform.position.x <= _leftBoundary)
        {           
            transform.position = new Vector3(_leftBoundary, transform.position.y, 0);
        }
    }

    private void PlayerMove()
    {
        if (_animate){
            transform.position = new Vector3(dirX * _speed + transform.position.x, transform.position.y, transform.position.z);
        }
        
        if (gameObject.transform.position.x <= _leftBoundary)
        {
            dirX = 1f;
        }
        else if (this.gameObject.transform.position.x >= _rightBoundary)
        {
            
            _animate = false;
            Wait();   
        }

    }
    private void Wait()
    {
        
        if (_timer < 5f)
        {
            _timer += Time.deltaTime;
        }
        else
        {
            dirX -= 1f;
            _timer = 0;
            _animate= true;
        }
    }

    private void SpriteFlip()
    {
        if (dirX == 1f)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (!_animate)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    private void SpriteAnimate()
    {
        if (_animate)
        {
            if (_counter % _frameRate == 0)
            {
                this.GetComponent<SpriteRenderer>().sprite = character[frameSprite];
                _counter = 0;
                if (frameSprite == 2)
                {
                    frameSprite = 0;
                }
                else
                {
                    frameSprite++;
                }
            }
            _counter++;
        }
        
    }

    private void CalculateSpeed()
    {
        _speed = (_speedAtenuador * Time.deltaTime) / Height;
    }

    private void BigHabilty()
    {
        _velocityBig = _bigAtenuator * Time.deltaTime;
        if (transform.localScale.x < _originalSize * 3)
        {
            transform.localScale += new Vector3(_velocityBig, _velocityBig, 0);
        }
        else
        {
            hG = HabilityGiant.GiantTime;
        }
    }

    private void DecHab()
    {
        _velocityBig = _bigAtenuator * Time.deltaTime;
        if (_originalSize < transform.localScale.x)
        {
            transform.localScale -= new Vector3(_velocityBig, _velocityBig, 0);
        }
        else
        {
            hG = HabilityGiant.Cooldown;
        }
    }
    public void TimerBig()
    {
        _sacleSize = false;
        if (_timerBig < BigDuration)
        {
            _timerBig += Time.deltaTime;
        }
        else
        {
            hG = HabilityGiant.Decrease;
            _timerBig = 0;
        }
    }
    public void Cooldown()
    {
        if (_timerBig < 10f)
        {
            _timerBig += Time.deltaTime;
        }
        else
        {
            _sacleSize = true;
            _timerBig = 0;
            hG = HabilityGiant.Nothing;
        }
    }

    /*private void GiantForm()
     {        
         _velocityBig = _bigAtenuator * Time.deltaTime;
         transform.localScale += new Vector3(1, 1, 0);
         if (transform.localScale.x > _originalSize && transform.localScale.x >= Height)
         {
             transform.localScale -= new Vector3(1, 1, 0);
         }
     }*/
}
