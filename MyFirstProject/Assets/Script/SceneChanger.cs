using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour
{
    private string txt;
    private GameObject input;
    private InputField inputFieldCo;
    private GameObject putName;
    private Text stringName;
    
    public void Change(){  
        input = GameObject.Find("input text");
        inputFieldCo = input.GetComponent<InputField>();
        txt = inputFieldCo.text;
        Debug.Log(txt);
        putName = GameObject.Find("name");
        stringName = putName.GetComponent<Text>();
        stringName.text = txt;
        if (txt.Length > 2)
        {
            SceneManager.LoadScene(1);
        }
        
    }
}
